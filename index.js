'use strict';

import Foxdriver from 'foxdriver';
import Emitter from 'events';
import queue from 'emitter-queue';
import express from 'express';
import bodyParser from 'body-parser';
import readline from 'readline';
import {Writable} from 'stream';

let fakeStdout = new Writable({
  write: (chunk, encoding, callback) => {
    callback();
  }
});

let silentReadline = readline.createInterface({
  input: process.stdin,
  output: fakeStdout,
  terminal: true
});

let promptEnter = async msg => {
  console.log(msg);
  await new Promise((resolve) => silentReadline.question('', resolve));
}

let messageQueue = queue(new Emitter());

let sendMessage = (browser, tab) => async msg => {
  await tab.console.evaluateJS(function(args) {
    document.querySelector('#textchat-input > textarea').value = args.msg;
    document.querySelector('#textchat-input > button').click();
  }, {msg});
  console.log('message sent');
};

let queueMessage = msg => messageQueue.queue('send', msg);

let messageSender = async () => {
  const { browser, tab } = await Foxdriver.launch({
    url: 'https://roll20.net',
    bin: './firefox.sh'
  });

  await tab.console.startListeners();

  await promptEnter('press enter once editor is open');
    
  console.log('sending queued messages')

  messageQueue.on('send', sendMessage(browser, tab));

  console.log('awaiting messages for sending');
};

let server = () => {
  let api = express();

  api.use(bodyParser.text());
  
  api.post('/msg', (req, res) => {
    res.setHeader('Connection', 'close');
    queueMessage(req.body);
    res.end();
  });
  
  api.listen(3000, () => console.log('listening for messages'));
};

messageSender();
server();
